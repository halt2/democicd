package com.example.demo.controller;

import com.example.demo.model.Student;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api")
public class HomeController {
    @Autowired
    StudentService studentService;
    @RequestMapping("/getall")
    public List<Student> getAll(){
        return (List<Student>) studentService.findAll();
    }

}
